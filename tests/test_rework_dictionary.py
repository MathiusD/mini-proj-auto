import unittest
from csv_process.reorder_csv import reorder_csv
from csv_process.split_column import split_column, date_format

class TestReworkDictionary(unittest.TestCase):
    
    def test_reorder(self):
        pattern = {'exemple':"",'example':""}
        example = {'1':"", 'example':3}
        verif = {'exemple':"",'example':3}
        self.assertEqual(verif, reorder_csv(example, pattern))

    def test_split(self):
        pattern = {
            'exemple':[{'action':"split",'delimiter':',','index':0,'data':'1'}],
            'example':[{'action':"split",'delimiter':',','index':1,'data':'1'}],
            'test':[{'action':'convert','data':'teste'}],
            'date':[{'action':"convert",'data':'dat'},{'action':"date_format",'data':'date','format_init':'%d/%m/%Y','format_final':'%Y-%m-%d'}]
        }
        example = {'1':"3,2", 'teste':3, 'dat':'01/04/2000'}
        verif = {'1':"3,2", 'teste':3,'dat':'01/04/2000','exemple':"3",'example':"2","test":3,"date":"2000-04-01"}
        self.assertEqual(verif, split_column(example, pattern))

    def test_date_format(self):
        self.assertEqual('01/04/2000', date_format('2000-04-01', '%Y-%m-%d', '%d/%m/%Y'))
        self.assertEqual('2000-04-01', date_format('01/04/2000', '%d/%m/%Y', '%Y-%m-%d'))

    def test_split_and_rework(self):
        pattern_convert = {
            'exemple':[{'action':"split",'delimiter':',','index':0,'data':'1'}],
            'example':[{'action':"split",'delimiter':',','index':1,'data':'1'}],
            'test':[{'action':'convert','data':'teste'}],
            'date':[{'action':"convert",'data':'dat'},{'action':"date_format",'data':'date','format_init':'%d/%m/%Y','format_final':'%Y-%m-%d'}]
        }
        pattern = {'exemple':"",'example':"",'test':"", 'date':""}
        example = {'1':"3,2", 'teste':3, 'dat':'01/04/2000'}
        verif = {'exemple':"3",'example':"2","test":3,"date":"2000-04-01"}
        self.assertEqual(verif, reorder_csv(split_column(example, pattern_convert), pattern))