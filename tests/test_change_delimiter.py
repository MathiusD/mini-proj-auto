import unittest
from csv_process.change_delimiter import replace_delimiter


class TestChangeDelimiter(unittest.TestCase): #Les noms de classe commencent impérativement par une majuscule
    def test_change_delimiter(self):
        doc = "bla,bla,bla"
        self.assertEqual("bla;bla;bla", replace_delimiter(doc, ',', ';'))
