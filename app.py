import argparse
from data.config_pattern import initial_delimiter, final_delimiter, pattern, pattern_convertion
from csv_process.reorder_csv import reorder_csv
from csv_process.split_column import split_column
from csv_process.change_delimiter import replace_delimiter
from csv_process.file_verif_open_write import verif_csv, open_csv, write_csv
#Définition des arguments pour appeler notre applicatif depuis la console.
parser = argparse.ArgumentParser(description='Converter for CSV files')
parser.add_argument("file", help="csv file to convert")
parser.add_argument("-output", help="name for output (Default : 'a.csv')")
parser.add_argument("-delimiteur", help="Delimiteur used (Default : ';')")
parser.add_argument("--same_file", help="Option for rewrite original file (True or False)")
parser.add_argument("--change_delimiteur_only", help="Option for change delimiteur only (True or False)")
args = parser.parse_args()

if (args.delimiteur != None):
    args.delimiteur = final_delimiter

if (verif_csv(args.file) == True):
    #Si les arguments sont bon et le fichier aussi on le traite
    if(args.change_delimiteur_only != 'True'):
        #Si on souhaite convertir le fichier
        data_in = open_csv(args.file, initial_delimiter)
        #Pour chaque donnée récupérée on la converti avant de la réordonner
        data_out = []
        first_row = True
        for row in data_in:
            if first_row == True:
                data = row
                first_row = False
            else:
                data = split_column(row, pattern_convertion)
            data = reorder_csv(data, pattern)
            data_out.append(data)
        #On prépare le nom de fichier de sortie
        if(args.output != 'True'):
            if(args.same_file != 'True'):
                path_output = "a.csv"
            else:
                path_output = args.file
        else:
            path_output = args.output
        #On écrit le tout
        write_csv(path_output, data_out, final_delimiter)
    else:
        #Si on souhaite que changer le délimiteur
        data_in = open(args.file)
        data_out = []
        for row in data_in:
            data_out.append(replace_delimiter(row, initial_delimiter, final_delimiter))
        file_write = open(args.file, "w")
        for row in data_out:
            file_write.write(row)
else:
    #Si le fichier est incorrect
    print("File is incorrect")