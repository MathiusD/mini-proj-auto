"""
    Fonction replace_delimiter
    %       Fonction qui remplace un délimiteur au sein d'une chaine
            de caractère par le délimiteur passé en argument.
    %IN     string_in : Chaine de caractère a modifier,
            delim_init : Caractère délimiteur à l'origine,
            delim_final : Caractère délimiteur souhaité
    %OUT    string_out : Chaine de caractère modifié
"""
def replace_delimiter(string_in, delim_init, delim_final):
    string_out = ""
    for i in range(len(string_in)):        
        if string_in[i] == delim_init:
            string_out += delim_final
        else:
            string_out += string_in[i]
    return string_out