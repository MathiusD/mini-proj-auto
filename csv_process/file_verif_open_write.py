import csv, os
"""
    Fonction verif_csv
    %       Cette fonction vérifie que le fichier existe que ce dernier
            est bien un fichier et qu'il se termine bien par '.csv'.
    %IN     path : Chaine de caractère représentant le chemin à un fichier
    %OUT    result : Booléen indiquant si le fichier est conforme ou non
"""
def verif_csv(path):
    if os.path.exists(path) and os.path.isfile(path) and path.split('.')[len(path.split('.')) - 1] == "csv":
        return True
    else:
        return False
"""
    Fonction open_csv
    %       Cette fonction lit un fichier csv passé en argument à l'aide du
            délimiteur passé en argument.
            Elle créé alors une liste de dictionnaire donc le premier est
            un pattern correspondant aux diverses catégories du fichier
            avant de compléter les autres dictionnaires selon ce pattern
            avec les données récoltées.
    %IN     path : Chaine de caractère représentant le chemin à un fichier,
            delimiteur : Caractère délimiteur au sein du fichier
    %OUT    list_data : Liste de dictionnaire correspondant au fichier csv
"""
def open_csv(path, delimiteur):
    pattern = {}
    list_data = []
    names = []
    with open(path) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        i = 0
        for row in reader:
            if len(pattern) == 0:
                for name in row:
                    pattern[name] = ""
                    names.append(name)
                list_data.append(pattern)                    
            else:
                list_data.append({})
                for indice in range(len(row)):
                    list_data[i][names[indice]] = row[indice]
            i = i + 1
    return list_data
"""
    Fonction touch
    %       Cette fonction créé un fichier avec le path passé en argument.
    %IN     path : Chaine de caractère représentant le chemin à un fichier
    %OUT    Rien
"""
def touch(path):
    with open(path, 'a'):
        os.utime(path, None)
"""
    Fonction write_csv
    %       Cette fonction écrit un fichier csv dont le chemin est passé en argument
            avec les données et le délimiteur passé en argument.
            Elle extrait donc les données de la liste de dictionnaires que nous lui
            passons en argument avec le premier dictionnaire qui est le pattern et
            on écrit donc les clés et ensuite on écrit les données de manières classique.
    %IN     path : Chaine de caractère représentant le chemin à un fichier,
            data : Liste de dictionnaire correspondant au fichier csv à écrire
            delimiteur : Caractère délimiteur au sein du fichier
    %OUT    Rien
"""
def write_csv(path, data, delimiteur):
    if (verif_csv == False):
        touch(path)
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=delimiteur)
        pattern_write = False
        for row in data:
            out=[]
            if pattern_write == False:
                for name in row:
                    out.append(name)
                pattern_write = True
            else:
                for name in row:
                    out.append(row[name])
            writer.writerow(out)