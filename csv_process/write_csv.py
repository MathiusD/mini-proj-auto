import csv

with open('example.csv', 'a') as csvfile: #a : append w : overwrite r : readonly
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])
    writer.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])