"""
    Fonction reorder_csv
    %       Cette fonction vérifie que le dictionnaire data_in correspond
            bien au pattern passé en argument.
            Si cela n'est pas le cas, il créé alors une copie où il ajoute
            les éléments manquant du pattern en les initiant avec une chaine
            de caractère vide. Ensuite il récupère les données dans la copie
            et les réordonne dans le même ordre que le pattern avant de renvoyer
            le dictionnaire ainsi produit.
            Cependant si le dictionnaire est conforme la fonction nous retourne
            alors le dictionnaire passé en argument.
    %IN     data_in : dictionnaire de données à traiter,
            pattern : dictionnaire de données servant de référence
    %OUT    data_out : dictionnaire de données conforme au pattern
"""
def reorder_csv(data_in, pattern):
    if pattern.keys() == data_in.keys():
        data_out = data_in
    else:
        data_temp = data_in
        for key in pattern.keys():
            if key not in data_temp:
                data_temp[key]=""
        data_out={}
        for key in pattern.keys():
            data_out[key]=data_temp[key]
    return data_out