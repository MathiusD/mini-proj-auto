import datetime
"""
    Fonction split_column
    %       Cette fonction convertit des données d'un dictionnaire, selon le pattern
            de convertion passé en argument.
            On copie en premier lieu le dictionnaire de données passé en arguments
            ensuite pour chaque clé présente dans le pattern de convertion on
            regarde quelle est l'action à efffectuer et on place la traitement de la
            donnée dans le dictionnaire à la même clé que celle du pattern de convertion
            (On la supplante si elle est déjà présente).
            Les 'actions' actuellement implémentées sont celle de convertion, celle de
            découpe et celle de formatage d'une date.
            La convertion replace la donnée à une autre clé.
            La découpe récupère l'index d'un colonne suite à son découpage selon un
            caractère de découpe.
            Le formatage d'une date récupère la date, ainsi que son format initial et
            le format final et envoie le tout à la fonction date_format.
    %IN     data_in : dictionnaire de données à traiter,
            pattern_convert : dictionnaire de données indiquant les manipulations
            de convertion
    %OUT    data_out : dictionnaire de données avec en plus les données converties
"""
def split_column(data_in, pattern_convert):
    data_out = data_in
    for key in pattern_convert.keys():
        for indice in range(len(pattern_convert[key])):
            if pattern_convert[key][indice]['action']=="convert":
                data_out[key] = data_out[pattern_convert[key][indice]['data']]
            elif pattern_convert[key][indice]['action']=="split":
                data_out[key] = data_out[pattern_convert[key][indice]['data']].split(pattern_convert[key][indice]['delimiter'])[pattern_convert[key][indice]['index']]
            elif pattern_convert[key][indice]['action']=="date_format":
                data_out[key] = date_format(data_out[pattern_convert[key][indice]['data']], pattern_convert[key][indice]['format_init'], pattern_convert[key][indice]['format_final'])
    return data_out
"""
    Fonction date_format
    %       Cette fonction formate la date passée en argument selon le format souhaité.
    %IN     date : Une chaine de caractère qui est la date initiale,
            format_init : Une chaine de caractère qui est le format de la date initiale,
            format_final : Une chaine de caractère qui est le format de la date souhaitée
    %OUT    date : Une chaine de caractère qui est la date ainsi convertie
"""
def date_format(date, format_init, format_final):
    return datetime.datetime.strptime(date, format_init).strftime(format_final)
